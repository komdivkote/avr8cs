/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "kernel_sectioning.h"
#include "kernel_memory_private.h"
#include "kernel_process_private.h"

/*Titles for the process status*/
#if (KERNEL_TRACK_PROC_STATE == YES)
const char KSTATE_SRAMALLOC[] PROGMEM	= "kbma/f";	//kernel basic memory alloc/free
const char KSTATE_SRAMIO[] PROGMEM		= "kbmio"; //kernel basic memory input output
#endif /* KERNEL_TRACK_PROC_STATE */

volatile uint8_t * heap_start			= 0;
volatile uint8_t * heap_end				= 0;
volatile block_size_t total_mem_avail	= 0;

#if KERNEL_MEM_CONT_RAM == YES
volatile block_size_t total_mem_used	= 0;
volatile block_size_t total_mem_descr	= 0;
volatile block_size_t total_mem_frag	= 0;
#endif //KERNEL_MEM_CONT_RAM

volatile struct memblock * volatile memblocks = NULL;

ATTRIBUTE_KERNEL_SECTION
void
__kern_resize_block(struct memblock * block, size_t newsize)
{
	block->addr = (block_size_t)block + newsize + sizeof(struct memblock);

	return;
}

ATTRIBUTE_KERNEL_SECTION
void *
__mem_allocate(struct memblock * block, size_t allocs)
{
    //check if new block does not reach bounds
	if ( ((block_size_t)block+allocs+sizeof(struct memblock)) > (block_size_t)heap_end)
	{
		//it is not possible to allocate free space
		return NULL;
	}

	//if block was not allocated previously
	if ( *((block_size_t *)block) == 0)
	{
		//allocate new
		__kern_resize_block(block, allocs);
#if KERNEL_MEM_CONT_RAM == YES
		++total_mem_descr;
		total_mem_used += allocs + sizeof(struct memblock);
#endif
	}
	else
	{
		//previously was in use
		//ToDo??? it is working, no additional workaround needed
#if KERNEL_MEM_CONT_RAM == YES
        total_mem_used += allocs;
#endif
	}

	//setting BUST flag
	block->mask |= MEM_FLAG_BUSY;

	//count used bytes with memblock header


	//return the pointer to the start of the heap
	return (void *) &block->heap;
}


ATTRIBUTE_KERNEL_SECTION
void *
__mem_try_allocate(struct memblock * prev_nearest_block, struct memblock * nearest_block, size_t allocs)
{
	block_size_t near_block_s = GET_BLOCK_SIZE_WITHOUT_HEAD(nearest_block); //(t_heap_size) ((uint8_t *) nearest_block->addr - (uint8_t *) nearest_block);

	//ToDo improve the allocation strategy
	//if the block length is larger then allocs + 2
	if (near_block_s > (allocs+sizeof(struct memblock)))
	{
		//setting next (new) block header
		struct memblock * blkf = (struct memblock *)((block_size_t)nearest_block + allocs + sizeof(struct memblock));
		block_size_t tbk = near_block_s - allocs - sizeof(struct memblock);

        //resizing new block (the space which was left)
		__kern_resize_block(blkf, tbk);
		//resize nearest_block which will be used
		__kern_resize_block(nearest_block, allocs);

#if KERNEL_MEM_CONT_RAM == YES
		//count memory used
		total_mem_used += allocs + sizeof(struct memblock);
#endif
		//set busy
		nearest_block->mask |= MEM_FLAG_BUSY;

		//return pointer to the start of the heap
		return (void *) &nearest_block->heap;
	}
	else if (near_block_s == (allocs+sizeof(struct memblock)))
	{
		//setting fragmentation block header at the end of the reused block
		struct memblock * blkf = (struct memblock *)((block_size_t) nearest_block + allocs + sizeof(struct memblock));
		blkf->mask = MEM_FLAG_FRAGMENT;
		blkf->addr = 0;

#if KERNEL_MEM_CONT_RAM == YES
		//increase fragmentation counter
		total_mem_frag += sizeof(struct memblock);

		//increase memory usage counter
		total_mem_used += allocs + sizeof(struct memblock);
#endif

		//setting current block to point to fragmentation block
		__kern_resize_block(nearest_block, allocs);

		//set current block busy
		nearest_block->mask |= MEM_FLAG_BUSY;

		//return pointer to the start of the heap
		return (void *) &nearest_block->heap;
	}

	return NULL;
}

ATTRIBUTE_KERNEL_SECTION
struct memblock *
__kern_reduce_block(struct memblock * blkc, size_t newsize)
{
	struct memblock * newblock = (struct memblock *)((block_size_t) blkc + newsize + sizeof(struct memblock));
	block_size_t tbk = (GET_BLOCK_SIZE_WITHOUT_HEAD(blkc)) - newsize; //+ sizeof(struct memblock));
	__kern_resize_block(newblock, tbk);
	__kern_resize_block(blkc, newsize);
	blkc->mask |= MEM_FLAG_BUSY;

	return newblock;
}

ATTRIBUTE_KERNEL_SECTION
struct memblock *
__kern_merge_blocks(enum kern_merge_type e_merge_type,
					struct memblock * blk_c,
					size_t blk_cs,
					size_t newsize,
					struct memblock * blk_pn,
					size_t blk_pns)
{
	//total new superblock length (previous_block_len - (header_len) + current_block_len)
	block_size_t totalnblock = ( blk_pns - sizeof(struct memblock)) + blk_cs;

	//calculating free bytes in new superblock that will be left unused
	block_size_t blockleft = totalnblock - newsize;

	struct memblock * blkf = NULL;

	switch (e_merge_type)
	{
		case K_MERGE_CURRENT_PREVIOUS:
            //setting next block header (previous + newsize + header)
            blkf = (struct memblock *)((block_size_t)blk_pn + newsize + sizeof(struct memblock));

            //move data from current to previous
            memmove( ((uint8_t *)blk_pn + sizeof(struct memblock)),
                     ((uint8_t *)blk_c + sizeof(struct memblock)),
                     blk_cs - sizeof(struct memblock) );
		break;

		case K_MERGE_CURRENT_NEXT:
            //setting next block header (previous + newsize + header)
            blkf = (struct memblock *)((block_size_t)blk_c + newsize + sizeof(struct memblock));
            blockleft -= sizeof(struct memblock);
		break;
	}

	//reset previously used
	memset(blkf, 0, blockleft);

	//if total_new_superblock_len > new_required_size + header_len
	if (totalnblock > (newsize+sizeof(struct memblock)))
	{
		//setting block headers
		__kern_resize_block(blkf, blockleft);

		switch (e_merge_type)
		{
			case K_MERGE_CURRENT_PREVIOUS:
                //resize previous block
                __kern_resize_block(blk_pn, newsize);

#if KERNEL_MEM_CONT_RAM == YES
                //updating counter: calculating memory used
                total_mem_used += (blk_cs - blockleft) + sizeof(struct memblock);
#endif

                //set busy
                blk_pn->mask |= MEM_FLAG_BUSY;

			break;

			case K_MERGE_CURRENT_NEXT:
                //resize current block
                __kern_resize_block(blk_c, newsize);

#if KERNEL_MEM_CONT_RAM == YES
                //updating counter: calculating memory used
                total_mem_used += (blk_pns - blockleft) + sizeof(struct memblock);
#endif

                //set busy
                blk_c->mask |= MEM_FLAG_BUSY;
			break;
		}


		//return pointer to the start of the heap
		return blkf;
	}
	else if (totalnblock == (newsize+sizeof(struct memblock)))
	{
		//setting 2 left bytes as a header type fragment
		blkf->mask = MEM_FLAG_FRAGMENT;
		blkf->addr = 0;

#if KERNEL_MEM_CONT_RAM == YES
		//increase fragmentation counter
		total_mem_frag += sizeof(struct memblock);

		//increase memory usage counter
		total_mem_used += newsize + sizeof(struct memblock);
#endif

		switch (e_merge_type)
		{
			case K_MERGE_CURRENT_PREVIOUS:
                //setting current block to point to fragmentation block
                __kern_resize_block(blk_pn, newsize);

                //set current block busy
                blk_pn->mask |= MEM_FLAG_BUSY;
			break;

			case K_MERGE_CURRENT_NEXT:
                //setting current block to point to fragmentation block
                __kern_resize_block(blk_c, newsize);

                //set current block busy
                blk_c->mask |= MEM_FLAG_BUSY;
			break;
		}

		//return pointer to the start of the heap
		return blkf;
	}

	return NULL;
}

ATTRIBUTE_KERNEL_SECTION
void *
__kern_use_near_block(struct memblock * block_near,
                        block_size_t block_near_len,
						size_t resize,
						struct memblock * ptr_block,
						block_size_t ptr_block_len)
{
	memmove(((uint8_t *)block_near+sizeof(struct memblock)), ((uint8_t *)ptr_block+sizeof(struct memblock)), ptr_block_len - sizeof(struct memblock));
	memset( ((uint8_t *)ptr_block+sizeof(struct memblock)), 0, ptr_block_len - sizeof(struct memblock));

	if (block_near_len > (resize+sizeof(struct memblock)))
	{
		//setting next block header
		struct memblock * blkf = (struct memblock *)((block_size_t)block_near + resize + sizeof(struct memblock));
		block_size_t tbk = block_near_len - resize - sizeof(struct memblock);

		__kern_resize_block(blkf, tbk);
		__kern_resize_block(block_near, resize);

#if KERNEL_MEM_CONT_RAM == YES
		total_mem_used += resize + sizeof(struct memblock);
#endif

		//set busy
		block_near->mask |= MEM_FLAG_BUSY;

		//return pointer to the start of the heap
		return (void *) &block_near->heap;
	}
	else if (block_near_len == (resize+sizeof(struct memblock)))
	{
		//setting fragmentation block header at the end of the reused block
		struct memblock * blkf = (struct memblock *)((block_size_t) block_near + resize + sizeof(struct memblock));
		blkf->mask = MEM_FLAG_FRAGMENT;
		blkf->addr = 0;

#if KERNEL_MEM_CONT_RAM == YES
		//increase fragmentation counter
		total_mem_frag += sizeof(struct memblock);

		//increase memory usage counter
		total_mem_used += resize + sizeof(struct memblock) + sizeof(struct memblock);
#endif

		//setting current block to point to fragmentation block
		__kern_resize_block(block_near, resize);

		//set current block busy
		block_near->mask |= MEM_FLAG_BUSY;

		//return pointer to the start of the heap
		return (void *) &block_near->heap;
	}

	return NULL;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
__kern_defragment(struct memblock * block_prev, struct memblock ** block)
{
	//performing check if previous_block presents, the address not equal to current(same) and previous_block is not busy
	if ((block_prev != NULL) &&
        (block_prev != (*block)) &&
        ((block_prev->mask & MEM_FLAG_BUSY) == 0))
	{
		//previous block is free, join prev with current
		struct memblock * block_next = GET_NEXT_BLOCK((*block));

		//check next block from current if it is allocated block or free
		//prev_block[free] -p-> current_block[free] -p-> next_block[?]
		if (*((block_size_t*)block_next) == 0)
		{
			//the header of the next block does not points to anywhere
			//the current is also free
			//so block_previous header now can point to nowhere because the higher
			//records did not point to anywhere (end)
			//prev_block -p-> 0
			block_prev->mask = 0;
			block_prev->addr = 0;
		}
		else
		{
			//the next record pointing somewhere (to next record), then set previous block,
			//point to where the current block was pointing!
			// was: prev_block[free] -p-> current_block[free] -p-> next_block[used] -p->?
			block_prev->addr = (*block)->addr;

			//now: prev_block[used] -p-> next_block[used] -p->? (current removed from chain)
		}

		//remove fragment mask, if it was set previously
		block_prev->mask &= ~MEM_FLAG_FRAGMENT;
		(*block)->mask = 0;
		(*block)->addr = 0;
		(*block) = block_prev;

#if KERNEL_MEM_CONT_RAM == YES
		--total_mem_descr;
		total_mem_used -= sizeof(struct memblock);
#endif
		return 1; //goto to start of the loop
	}

	return 0;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_memory_init(uint8_t * hstart, uint8_t * hend)
{
	heap_start = hstart;
	heap_end = hend;
	total_mem_avail =  (block_size_t)hend-(block_size_t)hstart + 1;
#if KERNEL_MEM_CONT_RAM == YES
	total_mem_used = 0;
	total_mem_descr = 0;
#endif

	memblocks = (struct memblock *) hstart;
	memset(memblocks, 0, total_mem_avail);

	return 0;
}

ATTRIBUTE_KERNEL_SECTION
void *
kernel_malloc(size_t allocs)
{
	MUTEX_BLOCK(memmux, MUTEX_ACTION_MANUAL)
	{
	//before the scheduler was started, there are no mutex is allocated, ignore it

	KERNEL_SET_STATE(KSTATE_SRAMALLOC)
	{
		//check if there is enough memory left
		if ((allocs+sizeof(struct memblock)) > total_mem_avail)
		{
			//not enough memory to allocate such large chunk
			return NULL;
		}

		struct memblock * block					= memblocks;	//current block
		struct memblock * block_prev			= NULL;			//prev block
		struct memblock * nearest_block			= NULL;			//var for deallocated block
		struct memblock * nearest_block_prev	= NULL;			//var for prev dealoc

		//iterate through the list memblocks
		while (1)
		{
			block_size_t total_block_size = 0;

			//calculating block size
			if (block->addr != NULL)
			{
				//the block is pointing to the next block
				total_block_size = GET_BLOCK_SIZE(block);
			}
			else
			{
				//the block is not pointing to the next block, the size is calculated from offset
				total_block_size = total_mem_avail - ((block_size_t)block - (block_size_t)heap_start);
			}

			if ((uint8_t *)block >= heap_end)
			{
				//out of RAM
				return NULL;
			}
			if (*((block_size_t*)block) == 0)
			{
				// reached end
				if (nearest_block != NULL)
				{
					//try to allocate found free block
					void * ret = __mem_try_allocate(nearest_block_prev, nearest_block, allocs);
					if (ret)
					{
						//return pointer to the heap of the nearest block
						return ret;
					}
				}

				//allocate new block
				return __mem_allocate(block, allocs);

			}
			else if ((block->mask & MEM_FLAG_BUSY) == 0)
			{
				//the current block is not busy
				//check if current block requires defrag
				if (__kern_defragment(block_prev, &block))
				{
					//the block was defragmented, continue
					continue;
				}

				//if current block is not a fragment
				if ((block->mask & MEM_FLAG_FRAGMENT) == 0)
				{
					//found free block, calculate block's heap size
					block_size_t paged = total_block_size - sizeof(struct memblock);
					//heap match exactly
					if (paged == allocs)
					{
						//allocate
						return __mem_allocate(block, allocs);
					}
					else if ((paged-sizeof(struct memblock)) >= allocs)
					{
						//else check if the current block can fit allocs and +2 bytes of the next header
						//store check if previously nearest_block was found
						if (nearest_block == NULL)
						{
							//setting
							nearest_block = block;
							nearest_block_prev = block_prev;
						}
						else
						{
							//check if actual block is smaller than the previously found
							block_size_t npage = GET_BLOCK_SIZE_WITHOUT_HEAD(nearest_block);
							if (npage > paged)
							{
								//smaller, then store this one
								nearest_block = block;
								nearest_block_prev = block_prev;
							}
						}
					}
				}
			}

			block_prev = block;
			block = (struct memblock *) ((block_size_t)block->addr);
		}

		return NULL;
	}//	KERNEL_SET_STATE
	}// MUTEX_BLOCK

	/*control reaches non-void warning*/
	return NULL;
}

ATTRIBUTE_KERNEL_SECTION
void *
kernel_realloc(void * ptr, size_t resize)
{
	MUTEX_BLOCK(memmux, MUTEX_ACTION_MANUAL)
	{
	//before the scheduler was started, there are no mutex is allocated, ignore it

	KERNEL_SET_STATE(KSTATE_SRAMALLOC)
	{
		//check if there is enough memory left
		if (ptr == NULL)
		{
			//not enough memory to allocate such large chunk
			return NULL;
		}

		struct memblock * block = memblocks;			//current block
		struct memblock * ptr_block = NULL;
		block_size_t ptr_block_len = 0;
		struct memblock * block_prev = NULL;			//prev block

		//searching for the header
		while (1)
		{
			block_size_t blocksize = GET_BLOCK_SIZE(block);

			//check if block ptr is larger than heap_end or block ptr equal to zero
			if ( ((uint8_t *)block >= heap_end) || (*((block_size_t*)block) == 0) )
			{
				//reached end
				return NULL;
			}
			else if (ptr == ((block_size_t)block+sizeof(struct memblock)))
			{
				//struct memblock * block_next = (struct memblock *) ((block_size_t)block + blocksize);
				struct memblock * block_next = (struct memblock *) block->addr;

				if ( (blocksize-sizeof(struct memblock)) == resize)
				{
					//??? what was the reason to call a resize
					return NULL; // was not resized
				}
				else if ( (blocksize-sizeof(struct memblock)) > resize)
				{
					//required to reduce current instance block size
					//__kern_resize_block(block, resize);
					(void)__kern_reduce_block(block, resize);

					//return pointer to the heap
					return (void *) &block->heap;
				}
				else
				{
					//try to extend the block using previous or next
					block_size_t blocksize_prev = 0;
					block_size_t blocksize_next = 0;

					//check if next block present and not used
					if ((*((block_size_t *)block_next) != 0) && ((block_next->mask & MEM_FLAG_BUSY) == 0))
					{
						//get size of the next block
						blocksize_next = GET_BLOCK_SIZE(block_next);
						//check if sum of 2 blocks is larger than requested size
						if ( ((int16_t)((blocksize_next + blocksize) - resize)) >= 0)
						{
							//use next
							__kern_merge_blocks(K_MERGE_CURRENT_NEXT ,block, blocksize, resize, block_next, blocksize_next);

							//return pointer to the heap
							return (void *) &block->heap;
						}
					}

					//check if previous block found, present and not used
					if ( (block_prev != NULL) && (*((block_size_t *)block_prev) != 0) && ((block_prev->mask & MEM_FLAG_BUSY) == 0) )
					{
						//get size of the previous block
						blocksize_prev = GET_BLOCK_SIZE(block_prev);

						//check if sum of 2 blocks is larger than requested size
						if ( ((int16_t)((blocksize_prev + blocksize) - resize)) >= 0)
						{
							//use prev
							__kern_merge_blocks(K_MERGE_CURRENT_PREVIOUS, block, blocksize, resize, block_prev, blocksize_prev);

							//return pointer to the heap
							return (void *) &block_prev->heap;
						}
					} //if

				} //if

				//record found pointer of the header of ptr
				ptr_block = block;
				ptr_block_len = blocksize;

				//leave loop
				break;
			} //if

			//move to next block
			block_prev = block;
			block = (struct memblock *) ((block_size_t)block->addr);
		} //while

		//during the first loop, the free space for reallocation was not found
		//run through the list again and search for the place in other blocks.
		//if it is not happen to find a free blocks then allocate new space and free last space
		block = memblocks;			//current block
		struct memblock * block_near = NULL;
		block_size_t block_near_len = 0;
		block_prev = NULL;

		while (1)
		{
			//get current block size
			block_size_t blocksize = GET_BLOCK_SIZE(block);

			if ((uint8_t *)block >= heap_end)
			{
				//out of RAM
				return NULL;
			}
			else if (*((block_size_t*)block) == 0) //reached end
			{
				void * nb = NULL;

				//if nearest by size was found then use it
				if (block_near != NULL)
				{
					nb = __kern_use_near_block(block_near, block_near_len, resize, ptr_block, ptr_block_len);
					if (nb)
					{
						//return pointer to the heap
						return nb;
					}
				}

				//else allocate new block
				nb = __mem_allocate(block, resize);
				if (nb)
				{
					//sub header length
					ptr_block_len -= sizeof(struct memblock);
					//copy data to new region
					memmove(nb, (const void *)&ptr_block->heap, ptr_block_len);
					//clear previous region
					memset((void *)&ptr_block->heap, 0, ptr_block_len);
					//set free
					ptr_block->mask &= ~MEM_FLAG_BUSY;
					//reduce memory used
#if KERNEL_MEM_CONT_RAM == YES
					total_mem_used -= ptr_block_len;
#endif
				}

				//return pointer to the heap
				return nb;
			}

			//check if block is not allocated
			if ( (block->mask & MEM_FLAG_BUSY) == 0)
			{
				//try to defragment it
				if (__kern_defragment(block_prev, &block))
				{
					//run once again loop from start
					continue;
				}

				//check that the current block is not same as argument ptr
				if (ptr != (void *) &block->heap )
				{
					if ((blocksize - sizeof(struct memblock)) == resize)
					{
						//found exact
						//copy data to new region
						memmove((void *) &block->heap, (const void *) &ptr_block->heap, ptr_block_len - sizeof(struct memblock));

						//clear  previous region
						memset((void *) &ptr_block->heap, 0, ptr_block_len - sizeof(struct memblock));

						//set new blocks's mask to busy
						block->mask |= MEM_FLAG_BUSY;

						//the original block is marked as free
						ptr_block->mask &= ~MEM_FLAG_BUSY;

						//return pointer to the heap
						return (void *) &block->heap;
					}
					else if ((blocksize - sizeof(struct memblock)) > resize)
					{
						//larger, then store it for future decision
						if (block_near == NULL)
						{
							block_near = block;
							block_near_len = blocksize;
						}
						else if (block_near_len > blocksize)
						{
							block_near = block;
							block_near_len = blocksize;
						}

					}

				} //if
			}//if

			block_prev = block;
			block = (struct memblock *) ((block_size_t)block->addr);
		}

		return NULL;
	}//	KERNEL_SET_STATE
	}//	MUTEX_BLOCK

	/*control reaches non-void warning*/
	return NULL;
}

ATTRIBUTE_KERNEL_SECTION
void
kernel_free(void * ptr)
{
	MUTEX_BLOCK(memmux, MUTEX_ACTION_MANUAL)
	{
	//before the scheduler was started, there are no mutex is allocated, ignore it

	KERNEL_SET_STATE(KSTATE_SRAMALLOC)
	{
	if (ptr == NULL)
	{
		return;
	}

#if KERNEL_MEM_USE_UNSAFE_FREE == YES

	//retrieving block record
	struct memblock * block = ptr - sizeof(struct memblock);

	//retreating page size
	block_size_t page = GET_BLOCK_SIZE_WITHOUT_HEAD(block);

	//retrieving next block header
	struct memblock * block_next = GET_NEXT_BLOCK(block);

	//if next block is NULL
	if (*((block_size_t *)block_next) == 0)
	{
		block->mask = 0;
		block->addr = 0;

#if KERNEL_MEM_CONT_RAM == YES
		//decrease memory usage counter
		total_mem_used -= (page + sizeof(struct memblock));

		//decrease descriptor
		--total_mem_descr;
#endif
	}
	else
	{
		block->mask &= ~MEM_FLAG_BUSY;
#if KERNEL_MEM_CONT_RAM == YES
		total_mem_used -= page;
#endif
	}

	//reset deallocated space
	memset(ptr, 0, page);

	return;

#else
	//setting pointer to the start of the list
	struct memblock * block = memblocks;

	//length of the current page
	block_size_t page = 0;

	//pointer to the data of the block
	uint8_t * sptr = 0;

	while ((block < heap_end) && ( *((block_size_t *) block) != 0))
	{
		//retrieving page size
		page = GET_BLOCK_SIZE_WITHOUT_HEAD(block);
		//counting pointer to data
		sptr = (block_size_t)block+sizeof(struct memblock);

		//if ptr has same address to current block's ptr
		if (ptr == sptr)
		{
			//if block is used
			if (block->mask & MEM_FLAG_BUSY)
			{
				struct memblock * block_next = GET_NEXT_BLOCK(block);

				if (*((block_size_t *) block_next) == 0)
				{
					block->mask = 0;
					block->addr = 0;
#if KERNEL_MEM_CONT_RAM == YES
					total_mem_used -= (page + sizeof(struct memblock));
					--total_mem_descr;
#endif
				}
				else
				{
					block->mask &= ~MEM_FLAG_BUSY;
#if KERNEL_MEM_CONT_RAM == YES
					total_mem_used -= page;
#endif
				}
				memset(sptr, 0, page);
				return;
			}

			//it is not busy
			return;
		}
		block = (struct memblock *) ((block_size_t)block->addr);
	}
#endif //KERNEL_MEM_USE_UNSAFE_FREE

	return;
	}//	KERNEL_SET_STATE
	}// MUTEX_BLOCK

	return;
}

ATTRIBUTE_KERNEL_SECTION
block_size_t
kernel_get_total_heap()
{
	return total_mem_avail;
}

#if KERNEL_MEM_CONT_RAM == YES
ATTRIBUTE_KERNEL_SECTION
block_size_t
kernel_get_total_heap_used()
{
	return total_mem_used;
}

ATTRIBUTE_KERNEL_SECTION
block_size_t
kernel_get_total_heap_descr()
{
	return total_mem_descr;
}

ATTRIBUTE_KERNEL_SECTION
block_size_t
kernel_get_total_heap_fragm()
{
	return total_mem_frag;
}
#endif
