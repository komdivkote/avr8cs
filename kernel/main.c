/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>
#include "kernel_preproc_defs.h"

//#include "kernel_types.h"
#include "kernel_memory.h"
#include "kernel_process.h"
#include "kernel_mutex.h"
#include "kernel_sem.h"
#include "kernel_fifo.h"
#include "kernel_isr.h"


#include <time.h>
#include <stdio.h>
#include "external/debug.h"

#define HEAP_ENDS_AT 5600

k_pid proc1 = K_NO_PID;
k_pid proc2 = K_NO_PID;
k_pid proc3 = K_NO_PID;
k_pid proc4 = K_NO_PID;
k_pid proc5 = K_NO_PID;

struct kproc_mutex * mux0;
struct kproc_sem * sem0;

void idle(void * arg)
{

	while (1)
	{
		for (uint8_t i=0; i < 100; i++)
		{
			asm volatile ("nop");
		}
	}
}

void process1(void * arg)
{
	DDRG |= (1 << DDG0);
	while (1)
	{

		kernel_mutex_lock(mux0);
		kernel_sem_enter(sem0);
		for (uint32_t i=0; i < 0x50000; i++)
		{
			asm volatile ("nop");
		}
		PORTG |= (1 << DDG0);
		for (uint32_t i=0; i < 0x50000; i++)
		{
			asm volatile ("nop");
		}

			PORTG &= ~(1 << DDG0);
		kernel_sem_leave(sem0);
		kernel_mutex_unlock(mux0);

	//kernel_process_usleep(10000, 0);

	/*struct kprocess env;
	kernel_processes_get_procinfo(3, &env);
	printf_P(PSTR("LEAVING! %u"), env.flags);*/
	}
	return;
}

void process2(void * arg)
{
	DDRG |= (1 << DDG2);
	while (1)
	{
		//kernel_sem_enter(sem0);

		MUTEX_BLOCK(mux0, MUTEX_ACTION_LEAVE)
		{
			kernel_sem_enter(sem0);
			for (uint32_t i=0; i < 0x50000; i++)
			{
				asm volatile ("nop");
			}
			PORTG |= (1 << PG2);
			for (uint32_t i=0; i < 0x50000; i++)
			{
				asm volatile ("nop");
			}
			PORTG &= ~(1 << PG2);
			kernel_sem_leave(sem0);
		}

		//kernel_sem_leave(sem0);
		//kernel_process_usleep(10000000);

	}
	return;
}

void process5(void * arg)
{
	while (1)
	{
		kernel_sem_enter(sem0);
		for (uint32_t i=0; i < 0x50000; i++)
		{
			asm volatile ("nop");
		}
		kernel_sem_leave(sem0);
		kernel_process_usleep(500000,0);

	}
	return;
}

void process_top(void * arg)
{
	 struct kprocess proc;
	cursoroff();
	clrscr();
	textcolor(YELLOW);
	textbackground(BLUE);
	gotoxy(1,4);
	clreol();
	printf_P(PSTR("PID"));
	gotoxy(8,4);
	printf_P(PSTR("STS"));
	gotoxy(16,4);
	printf_P(PSTR("STC"));
	gotoxy(24,4);
	printf_P(PSTR("STE"));
	gotoxy(32,4);
	printf_P(PSTR("MEM%%"));
	gotoxy(40,4);
	printf_P(PSTR("CPU%%"));
	gotoxy(48,4);
	printf_P(PSTR("FLAGS"));
	gotoxy(56,4);
	printf_P(PSTR("STATE"));
	gotoxy(64,4);
	printf_P(PSTR("TITLE"));

	while(1)
	{
		uint16_t pids = kernel_processes_get_amount();
        k_iid isrs = kernel_isr_get_amount();

		cursoroff();
		gotoxy(1,1);
		textbackground(BLACK);
		clreol();
		textcolor(YELLOW);
		textbackground(BLUE);

		uint8_t proc_cpu = kernel_get_proc_cpu_utiliz();
		uint32_t proc_isr = kernel_get_isr_cpu_msec();
		uint16_t sa = (uint16_t)kernel_get_common_stack_address() - HEAP_ENDS_AT;
		printf_P(PSTR("Processes: %u CPU: %u%% ISR: %lu(ms) SYS:%u%%\r\n"), pids, proc_cpu, proc_isr, 100-proc_cpu);
		printf_P(PSTR("Memory: TH:%uB HA:%uB MD:%u Frag:%uB CSSS:%uB\r\n"), kernel_get_total_heap(), kernel_get_total_heap_used(), kernel_get_total_heap_descr(), kernel_get_total_heap_fragm(), sa);
		//time
		gotoxy(62, 1);
		time_t ms = kernel_get_seconds();
		struct tm * tmm = gmtime(&ms);
		printf_P(PSTR("%u:%u:%u:%u"), (tmm->tm_mday-1), tmm->tm_hour, tmm->tm_min, tmm->tm_sec);

		//mutexes
		gotoxy(1, 6+pids+isrs);
		printf_P(PSTR("MUTEXES"));
		gotoxy(1, 10+pids+isrs);
		printf_P(PSTR("SEMAPHORES"));

		textcolor(WHITE);
		textbackground(BLACK);
		uint8_t y_off = 5;
		for (uint16_t pid = 0; pid < pids; pid++)
		{
			int8_t status = kernel_processes_get_procinfo(pid, &proc);
			if (status != 0)
			{
				continue;
			}

			gotoxy(1,y_off);
			clreol();
			printf_P(PSTR("%d"), proc.pid);
			gotoxy(8,y_off);
			printf_P(PSTR("%u"), proc.stack_start);
			gotoxy(16,y_off);
			printf_P(PSTR("%u"), proc.stack_saved);
			gotoxy(24,y_off);
			printf_P(PSTR("%u"), proc.stack_end);
			gotoxy(32,y_off);
			div_t mper = div(( (proc.stack_start - proc.stack_saved)*100 ),(proc.stack_start - proc.stack_end + 1));
			printf_P(PSTR("%u%%"), (uint16_t)mper.quot);
			gotoxy(40,y_off);
			printf_P(PSTR("%u%%"), proc.cpu_usage);
			gotoxy(48,y_off);
			char fr = (proc.flags & KPROC_FLAG_RUN) ? 'R' : '\0';
			char fs = (proc.flags & KPROC_FLAG_SLEEP) ? 'S' : '\0';
			char fw = (proc.flags & KPROC_FLAG_WAIT) ? 'W' : '\0';
			char fc = (proc.flags & KPROC_FLAG_COOP) ? 'C' : 'P';
			char fpm = (proc.flags & KPROC_FLAG_TYPE_SYSTEM) ? '+' : '\0';
			printf_P(PSTR("%c%c%c%c%c"), fr, fs, fw, fc, fpm);
			gotoxy(56,y_off);
			printf_P(PSTR("%S"), proc.state);
			gotoxy(64,y_off);
			printf_P(PSTR("%S"), proc.title);
			y_off++;
		}

        textcolor(RED);

		struct kisr ksr = {0};
		for (k_iid pid = 0; pid < isrs; pid++)
		{
		    int8_t status = kernel_isr_get_info(pid, &ksr);
		    if (status != 0)
            {
                gotoxy(1,y_off);
                clreol();
                printf_P(PSTR("ISR:internal error"));
                y_off++;
                continue;
            }

            gotoxy(1,y_off);
			clreol();
			printf_P(PSTR("ISR%d"), ksr.isr_num);
			gotoxy(16,y_off);
			printf_P(PSTR("N/A"));
            gotoxy(25,y_off);
			printf_P(PSTR("%lu(ms)/calls:%lu"), kernel_isr_tcnt2ms(ksr.tcnt_cnt), ksr.total_cals);
			gotoxy(48,y_off);
            //char in = (ksr.flags.flags & KISRF_ISR) ? 'I' : '-';
            char su = (ksr.flags.flags & KISRF_SUSPEND) ? 'S' : '-';
            char de = (ksr.flags.flags & KISRF_DELAYED) ? 'D' : '-';
            printf_P(PSTR("%u%c%c"), ksr.flags.cnt, su, de);
            gotoxy(56,y_off);
			printf_P(PSTR("%p"), ksr.ISR_HANDLER);
            gotoxy(64,y_off);
            printf_P(PSTR("%S"), kernel_isr_get_caption(ksr.isr_num));

            y_off++;
		}

        textcolor(WHITE);

			if (mux0 != NULL)
			{
				gotoxy(1, 6+pids+1+isrs);
				clreol();
				printf_P(PSTR("Owner:%u wating:%u order:%u counter%u"), kernel_mutex_get_cnt(mux0), kernel_mutex_get_cwating(mux0), kernel_mutex_get_corder(mux0), kernel_mutex_get_ccounter(mux0));
			}

			if (sem0 != NULL)
			{
				gotoxy(1, 10+pids+1+isrs);
				clreol();
				printf_P(PSTR("SID:%"PRIu8" CA:%"PRIu8" FR:%"PRIu8" WA:%"PRIu8), 0, kernel_sem_get_total(sem0), kernel_sem_get_free(sem0), kernel_sem_get_waiting(sem0));
			}


		kernel_process_usleep(1000000, 0);
	}
}

extern uint8_t __heap_start;
extern uint8_t __heap_end;

void int0_isr();
void int0_isr()
{
    _delay_ms(4);
}

void int1_isr();
void int1_isr()
{
    _delay_ms(15);
}

int main(void)
{
    /* Replace with your application code */
	//%PID							%TITLE		%RESERVED	%STACK SIZE %PROCESS %ARGUMENT

	DDRD &= ~(1 << PD0) & ~(1 << PD1);
	PORTD |= (1 << PD0) | (1 << PD1);

	MCU_Init();
	kernel_memory_init((uint8_t *)&__heap_start, (uint8_t *)((uint16_t)&__heap_start+(uint16_t)HEAP_ENDS_AT));
	
	proc1 = kernel_create_process(PSTR("idle"), KPROC_TYPE_SYSTEM, 60, idle, NULL);			//PID = 0 running only when the rest are sleeping, waiting or stoped
	proc2 = kernel_create_process(PSTR("task1"), KPROC_TYPE_SYSTEM, 300, process1, NULL);
	proc3 = kernel_create_process(PSTR("task2"), KPROC_TYPE_SYSTEM, 300, process2, NULL);
	proc4 = kernel_create_process(PSTR("top"), KPROC_TYPE_SYSTEM, 200, process_top, NULL);
	proc5 = kernel_create_process(PSTR("task4"), KPROC_TYPE_SYSTEM, 160, process5, NULL);
	mux0 = kernel_mutex_init();
	sem0 = kernel_sem_init(2);
	kernel_isr_grab(INT0_vect_num, int0_isr, ISR_NORMAL);
	EICRA = (1 << ISC01);
	EIMSK = (1<<INT0);
	kernel_isr_init();
	kernel_isr_grab(INT1_vect_num, int1_isr, ISR_DELAYED);
	EICRA |= (1 << ISC11);
	EIMSK |= (1<<INT1);
	kernel_processes_start();

    while (1)
    {
    }
}

