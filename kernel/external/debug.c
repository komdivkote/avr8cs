/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <inttypes.h>
#include <avr/pgmspace.h>

#include "../kernel_types.h"
#include "../kernel_process_private.h"
#include "debug.h"


const char hexchars[16] = "0123456789ABCDEF";
const char * memview_fkeys[10] = {"Help",0,0,0,"DynMem",0,0,0,0,"Quit"};

void kernel_memviewer(void *start);
void drawmem(const unsigned char *mem);

void kernel_debug(uint8_t * sp_bk)
{
	gotoxy(1, 9);
	printf_P(PSTR("stack pointer: %x sreg: %x\r\n"), sp_bk, SREG);
	#if (KERNEL_TRACK_PROC_STATE == YES)
	printf_P(PSTR("current task: %u state %S\r\n"),current_kprocess->node->pid, current_kprocess->node->state);
	#else
	printf_P(PSTR("current task: %u \r\n"),current_kprocess->node->pid);
	#endif
	printf_P(PSTR("PRESS ANY KEY TO ENTER MEMVIEWER\r\n"));

	_getch();
	//debug
	kernel_memviewer(sp_bk);

}

void drawmem(const unsigned char *mem)
{
	const unsigned char *mptr;
	int i,j;
	textbackground(BLUE);
	for (i=0;i<23 && !_kbhit();i++) {
		mptr = &mem[i*16];
		gotoxy(1,i+2);
		textcolor(YELLOW);
		j = 8;
		do {
			j--;
			_putch(hexchars[  (((int)mptr)>>(j*4)) &0xF] );
		} while (j>0);
		_cputs(":  ");
		textcolor(WHITE);

		for (j=0;j<16;j++) {
			_putch(hexchars[*mptr>>4]);
			_putch(hexchars[*mptr&0xF]);
			mptr++;
			_putch(' ');
			if (j == 7) {
				_putch('-');
				_putch(' ');
			}
		}

		_putch(179);
		_putch(' ');

		mptr = &mem[i*16];
		for (j=0;j<16;j++) {
			if (*mptr >= ' ')
			_putch(*mptr);
			else _putch('.');
			mptr++;
		}

		clreol();
	}
}


void kernel_memviewer(void *start)
{
	int key;
	const unsigned char *mem;

	textattr(LIGHTGRAY  | BLUE <<4);
	clrscr();
	textcolor(BLACK);
	textbackground(CYAN);
	_cputs("Memory viewer");
	clreol();

	cursoroff();
	drawfkeys(memview_fkeys);

	mem=(const unsigned char *)start;
	for (;;) {
		drawmem(mem);

		//   if (_kbhit()) {
		key = _getch();
		switch(key) {
			case KB_LEFT: if ((int)mem > 0 ) (int)mem--; break;
			case KB_RIGHT: mem++; break;
			case KB_UP: if (mem >= (unsigned char const *)16) mem-=16; break;
			case KB_DOWN: mem+=16; break;
			case KB_PGUP: if (mem >= (unsigned char const *)(16*23))  mem-=16*23; break;
			case KB_PGDN: mem+=16*23; break;
			case KB_F10: cursoron(); return ;
		}
		// }

	}
}


