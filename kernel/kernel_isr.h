/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#ifndef KERNEL_ISR_H_INCLUDED
#define KERNEL_ISR_H_INCLUDED

#include "kernel_types.h"
#include "kernel_preproc_defs.h"

#if KERNEL_BUILD_WITH_ISR == YES

//#define KISRF_ISR 0x01          //ISR flag
#define KISRF_SUSPEND 0x01      //ISR suspend
#define KISRF_DELAYED 0x02      //ISR delayed interrupt handling

struct kisr_flags
{
    uint8_t cnt : 6;
    uint8_t flags : 2;
};

struct kisr
{
    uint8_t isr_num;            //ISR number
    //uint8_t isr_reqs;           //flags
    struct kisr_flags flags;
    void (*ISR_HANDLER)();      //handler

    struct kisr * next;         //pointer to next record in chain
#if KERNEL_MEASURE_PERF==YES
    uint32_t total_cals;        //total amount of calls
    uint32_t tcnt_cnt;
#endif // KERNEL_MEASURE_PERF
};

/*
 * enum isr_type
 * Defines what type of ISR will be used.
 */
enum isr_type
{
    ISR_NORMAL  = (uint8_t) 0,
    ISR_DELAYED = (uint8_t) 1
};

/*
 * kernel_isr_init
 * This function should be called if the delayed processed ISR was declared.
 *  This function will create process with title [ISR_ITERATOR], which will
 *  periodically scan the system ISR table.
 */
int8_t kernel_isr_init();

/*
 * kernel_isr_bind
 * This function initializes record to catch ISR.
 * isr_id (k_iid) ISR vector ID
 * ISR_HANDLER (*) pointer to the handler function
 * dp (uint8_t) delayed processing 0-disabled 1-enabled
 * returns (int8_t) RET_OK or RET_FAIL
 */
extern int8_t kernel_isr_grab(k_iid isr_id, void (*ISR_HANDLER)(), enum isr_type e_itype);

/*
 * kernel_isr_release
 * This function releases ISR routine by removing record form system vtable.
 * isr_id (k_iid) the ISR vector ID
 * returns (int8_t) RET_OK or RET_FAIL
 */
extern int8_t kernel_isr_release(k_iid isr_id);

/*
 * kernel_isr_suspend
 * This function will not disable specified in the argument interrupt,
 *  because it has no idea which register in the I/O of hardware to change.
 *  This function will only set flag suspen, which will prevent ISR routine exec.
 * isr_id (k_iid) the ISR vector ID
 * returns (int8_t) RET_OK or RET_FAIL
 */
extern int8_t kernel_isr_suspend(k_iid isr_id);

/*
 * kernel_isr_restore
 * This function restores ISR from 'virtual' suspend.
 * isr_id (k_iid) the ISR vector ID
 * returns (int8_t) RET_OK or RET_FAIL
 */
extern int8_t kernel_isr_restore(k_iid isr_id);

/*
 * kernel_isr_get_amount
 * This function returns amount of ISR records in system ISR table.
 * returns (int8_t) table size
 */
int8_t kernel_isr_get_amount();

/*
 * kernel_isr_get_info
 * This function copies the content of the system table to local variable
 *  from the system ISR table by the isr_rec_ord selector.
 * isr_rec_ord (k_iid) the order number (not vector number)
 * isrv (struct kirv *) valid pointer to the local variable
 */
int8_t kernel_isr_get_info(k_iid isr_rec_ord, struct kisr * isrv);

/*
 * kernel_isr_get_caption
 * THis function returns caption of the ISR retreiving it
 *  from pre-defined caption table located at PROGMEM.
 * isr_id (k_iid) the ISR vector ID
 * returns: pointer to the string located in PROGMEM
 */
extern PGM_P kernel_isr_get_caption(k_iid isr_id);

#endif // KERNEL_BUILD_WITH_ISR

#endif // KERNEL_ISR_H_INCLUDED
