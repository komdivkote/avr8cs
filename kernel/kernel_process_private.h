/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef KERNEL_PRIVATE_H_
#define KERNEL_PRIVATE_H_

#include "kernel_preproc_defs.h"
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "kernel_process.h"


/*kernel process mutex structure declaration*/
struct kproc_mutex
{
	struct kprocess * owner;		//owner instance
	//struct kprocess * prev_owner;	//previous owner
	uint8_t c_waiting;				//waiting for
	uint8_t c_mutx_order;			//counting order
	uint8_t c_mutx_counter;			//counter of the wakeup order
};

/*kernel process semaphore structure declaration*/
struct kproc_sem
{
    uint8_t sem_amnt;       //total amount
    uint8_t sem_lcnt;       //left free blocks
    uint8_t sem_wcnt;       //processes waiting for sem
};

/*kernel process FIFO buffer*/
struct kproc_fifo
{
    uint8_t * fifo_start;        //start address
    uint8_t * fifo_end;          //end address (i.e 00h->0Ah = 10 elements from 0..9)
    uint8_t * fifo_write;        //write pos
    uint8_t * fifo_read;         //read pos
};

#include "kernel_mutex.h"
#include "kernel_sem.h"
#include "kernel_fifo.h"

//sleep info
struct kproc_sleep
{
	uint8_t tcnt_start;								//the TCNT0 value
	uint32_t ocr0g;									//the OCR timer value
	uint8_t flags;
	struct kprocess_list * next_sleeping_kprocess;	//next sleeping proc in chain
};

#if KERNEL_BUILD_WITH_CONDITION_WAIT == YES

struct kproc_wait_condition
{
    uint8_t * cvar;
    enum simple_condition_eq e_sceq;
};

#endif // KERNEL_BUILD_WITH_CONDITION_WAIT

//processes list structure
struct kprocess_list
{
	struct kprocess * node;				        //the process node
	struct kproc_msg * wait_msg;		        //a pending message
	struct kproc_mutex * wait_mutex;	        //waitng for mutex to be unlocked
	struct kproc_sem * wait_sem;                //waiting for semaphore
	uint8_t mutex_wait_order;			        //the order to call
#if KERNEL_BUILD_WITH_CONDITION_WAIT == YES
	struct kproc_wait_condition * wait_cond;    //wake up task when condition is true
#endif // KERNEL_BUILD_WITH_CONDITION_WAIT
	struct kproc_sleep * sleep;			        //sleep info
	struct kprocess_list * next;		        //next process container in chain
};

#if (KERNEL_TRACK_PROC_STATE == YES)
extern const char KSTATE_SLEEP[]		PROGMEM;
extern const char KSTATE_MUX[]			PROGMEM;
extern const char KSTATE_MSG[]			PROGMEM;
extern const char KSTATE_KERN[]			PROGMEM;
extern const char KSTATE_WAIT[]         PROGMEM;
extern const char KSTATE_CPU[]			PROGMEM;
extern const char KSTATE_SRAMALLOC[]	PROGMEM;
extern const char KSTATE_SRAMIO[]		PROGMEM;
extern const char KSTATE_SEM[]          PROGMEM;
extern const char KSTATE_FIFO[]         PROGMEM;
#else
    #define KSTATE_SLEEP
    #define KSTATE_MUX
    #define KSTATE_MSG
    #define KSTATE_KERN
    #define KSTATE_CPU
    #define KSTATE_SRAMALLOC
    #define KSTATE_SRAMIO
    #define KSTATE_SEM
    #define KSTATE_FIFO
#endif /*  KERNEL_TRACK_PROC_STATE */

#if (KERNEL_MEASURE_PERF==YES)
    void _isr_kernel_process_count_time_spent();
    void _isr_kernel_process_start_measure_time();
    void _isr_kernel_cpu_utilization_eval();
#endif

#if KERNEL_BUILD_WITH_ISR == YES
    #include "kernel_isr.h"
    //ISR interrupts register
    extern volatile struct kisr * volatile isr_list;


    /*
     * kernel_isr_get
     * This function returns the record from the isr_list file.
     * isr_id (k_iid) the ISR vector number
     * returns: valid pointer to the record or NULL
     */
    extern struct kisr * kernel_isr_get(k_iid isr_id);

    extern volatile uint8_t isr_to_process;

#if (KERNEL_MEASURE_PERF_ISR == YES)
    extern void _isr_bind_timer1();
    extern void _isr_kern_measure_isr();
    extern void _isr_kern_count_isr(struct kisr * kisrv);
#endif
#endif // KERNEL_BUILD_WITH_ISR





extern volatile k_pid lpid;								//last pid
extern volatile uint16_t milis_cnt;						//milliseconds counter
extern volatile k_sec seconds_cnt;						//seconds counter
//extern volatile k_ms ocr0a_ovflws;					//overflows counter of the ocr0a
extern volatile uint8_t cpu_utiliz_total;				//total % processes CPU used by processes
extern volatile k_pid pid_cnt;						    //counter of processes
extern volatile k_proc_stack * volatile mainStackaddr;	//the main() stack adress space
extern volatile k_proc_stack ** volatile selectedStack;	//selected stack by contex_switching

#if (KERNEL_MEASURE_PERF==YES)
#if (KERNEL_MEASURE_PERF_ISR == YES)
extern volatile uint32_t isr_tcnt_cnt;                         //ISR miliseconds accomulator
extern volatile uint32_t isr_tcnt_counted;                     //ISR CPU usage in (%)

#endif
#endif

//process list
extern volatile struct kprocess_list * volatile kproc_list;
//current running process
extern volatile struct kprocess_list * volatile current_kprocess;
//the nearest to wakeup sleeping process
extern volatile struct kprocess_list * volatile sleeping_kprocess;
//the nearest to wakeup sleeping process
extern volatile struct kprocess_list * volatile sleeping_kprocess_list;

//void __set_proc_state(const char * state);

/*-----------------------------------------------------------*/

/*
 * Storing all registers to the RAM.
 * After R0 is pushed, the SREG is stored to RO register i.e sreg = SREG; and
 *		the interupts are disabled by the cli command. From this point no other
 *		interrupt will not grab the control.
 *		In the port of FREERTOS said that the R1 should be cleared, but it is
 *		up to the developer.
 *		In last section the "top of the stack" is updated, from where the
 *		RESTORE macros will perform the restore of the stack.
 */

#define STORE_REGISTERS()									\
	asm volatile (	"push	r31						\n\t"	\
					"push	r0						\n\t"	\
					"in		r0, __SREG__			\n\t"	\
					"push	r0						\n\t"	\
					"push	r1						\n\t"	\
					"clr	r1						\n\t"	\
					"push	r2						\n\t"	\
					"push	r3						\n\t"	\
					"push	r4						\n\t"	\
					"push	r5						\n\t"	\
					"push	r6						\n\t"	\
					"push	r7						\n\t"	\
					"push	r8						\n\t"	\
					"push	r9						\n\t"	\
					"push	r10						\n\t"	\
					"push	r11						\n\t"	\
					"push	r12						\n\t"	\
					"push	r13						\n\t"	\
					"push	r14						\n\t"	\
					"push	r15						\n\t"	\
					"push	r16						\n\t"	\
					"push	r17						\n\t"	\
					"push	r18						\n\t"	\
					"push	r19						\n\t"	\
					"push	r20						\n\t"	\
					"push	r21						\n\t"	\
					"push	r22						\n\t"	\
					"push	r23						\n\t"	\
					"push	r24						\n\t"	\
					"push	r25						\n\t"	\
					"push	r26						\n\t"	\
					"push	r27						\n\t"	\
					"push	r28						\n\t"	\
					"push	r29						\n\t"	\
					"push	r30						\n\t"	\
					"lds	r26, selectedStack		\n\t"	\
					"lds	r27, selectedStack + 1	\n\t"	\
					"in		r0, __SP_L__			\n\t"	\
					"st		x+, r0					\n\t"	\
					"in		r0, __SP_H__			\n\t"	\
					"st		x+, r0					\n\t"	\
				);

/*
 * The top of the stack located at some point in memory of the MCU is
 *	loaded to X register where the indirect reading with X post increment to Y is
 *	performed and I/O write to CPU register SP.
 */

#define RESTORE_REGISTERS()								\
	asm volatile (	"lds	r26, selectedStack		\n\t"	\
					"lds	r27, selectedStack + 1	\n\t"	\
					"ld		r28, x+					\n\t"	\
					"out	__SP_L__, r28			\n\t"	\
					"ld		r29, x+					\n\t"	\
					"out	__SP_H__, r29			\n\t"	\
					"pop	r30						\n\t"	\
					"pop	r29						\n\t"	\
					"pop	r28						\n\t"	\
					"pop	r27						\n\t"	\
					"pop	r26						\n\t"	\
					"pop	r25						\n\t"	\
					"pop	r24						\n\t"	\
					"pop	r23						\n\t"	\
					"pop	r22						\n\t"	\
					"pop	r21						\n\t"	\
					"pop	r20						\n\t"	\
					"pop	r19						\n\t"	\
					"pop	r18						\n\t"	\
					"pop	r17						\n\t"	\
					"pop	r16						\n\t"	\
					"pop	r15						\n\t"	\
					"pop	r14						\n\t"	\
					"pop	r13						\n\t"	\
					"pop	r12						\n\t"	\
					"pop	r11						\n\t"	\
					"pop	r10						\n\t"	\
					"pop	r9						\n\t"	\
					"pop	r8						\n\t"	\
					"pop	r7						\n\t"	\
					"pop	r6						\n\t"	\
					"pop	r5						\n\t"	\
					"pop	r4						\n\t"	\
					"pop	r3						\n\t"	\
					"pop	r2						\n\t"	\
					"pop	r1						\n\t"	\
					"pop	r0						\n\t"	\
					"out	__SREG__, r0			\n\t"	\
					"pop	r0						\n\t"	\
					"pop	r31						\n\t"	\
				);

/*
 * RESTORE_TIMER_STACK sets the stackpointer at the top of the main() stack.
 */
#define RESTORE_TIMER_STACK()							\
	asm volatile (	"lds	r26, mainStackaddr		\n\t"	\
					"lds	r27, mainStackaddr + 1	\n\t"	\
					"out	__SP_L__, r26			\n\t"	\
					"out	__SP_H__, r27			\n\t"	\
				);

/*----REGISTER FILE DUMP/RESTORE AND SHEDULING STARTS HERE---------------------------------------*/


/*----------------------STATES------------------------------------------*/
#if (KERNEL_TRACK_PROC_STATE == YES)
uint8_t __set_proc_state(const char * state);
const char * __get_proc_state(void);

struct sstate
{
	const char * prg_state_prev_ptr;
	uint8_t ret;
};

static inline void
__restore_state(const struct sstate * sst)
{
	if (sst->prg_state_prev_ptr != NULL)
	{
		__set_proc_state(sst->prg_state_prev_ptr);
	}

	asm volatile ("" ::: "memory");
}

#define KERNEL_SET_STATE(_STATE_) \
	for (struct sstate st __attribute__((__cleanup__(__restore_state))) = {__get_proc_state(), __set_proc_state(_STATE_)};\
		st.ret;\
		st.ret = 0)
#else
    #define KERNEL_SET_STATE(_STATE_) \
	for (uint8_t _ToDo_ = 1; _ToDo_; _ToDo_ = 0)
#endif /* KETNEL_TRACK_PROC_STATE */

#endif /* KERNEL_PRIVATE_H_ */
