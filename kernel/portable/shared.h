/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#ifndef SHARED_H_INCLUDED
#define SHARED_H_INCLUDED

#if (KERNEL_MEASURE_PERF==YES)
#define _ISR_MEASURE_E() _isr_kern_measure_isr()
#define _ISR_MEASURE_L(_isr_vec_ptr_) _isr_kern_count_isr(_isr_vec_ptr_)
#else
#define _ISR_MEASURE_E()
#define _ISR_MEASURE_L(_isr_vec_ptr_)
#endif

/*
 * The default code for each ISR
 */

/*
 * default_isr_code_section
 * A defualt code which is executed in every ISR sections.
 */
void default_isr_code_section(uint8_t _vec_num_, struct kisr ** isr_ptr);

#define DEFAULT_ISR_CODE_SECTION(_vec_num_)\
    static struct kisr * isr_ptr = NULL;\
	STORE_REGISTERS();\
	RESTORE_TIMER_STACK();\
    default_isr_code_section(_vec_num_, &isr_ptr);\
	RESTORE_REGISTERS();\
	asm volatile ( "reti" );


#endif // SHARED_H_INCLUDED
