/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef KERNEL_MEMORY_H_
#define KERNEL_MEMORY_H_

#include "kernel_preproc_defs.h"

/*
 * MEM_FLAG_FRAGMENT - a structure mask fields flag, which indicated that the block is a fragment and
 *						can not be used to store data (usually have no heap space and points immidiatly
 *						to the next record)
 * MEM_FLAG_BUSY - a structures mask fields flag, which indicated that the block is allocated and used.
 *					if the structure mask field is not set but the addr field is pointing to the address
 *					means that the block was deallocated and ready to be reused.
 */

#define MEM_FLAG_FRAGMENT 1
#define MEM_FLAG_BUSY 2

typedef uint16_t block_size_t;

/*
 * kernel_memory_init
 * This function initializes memory region which will be used for heap.
 * Without running this function, calling kernel_malloc will result in unexpected behaviour.
 * hstart (uint8_t *) start of the heap address (usually it is (uint8_t *)&__heap_start)
 * hend (uint8_t *) the end of the region
 * return: always return 0
 */
int8_t kernel_memory_init(uint8_t * hstart, uint8_t * hend);

/*
 * kernel_malloc
 * This functions allocates a chunk of memory of requested length.
 * allocs (size_t) required size
 * returns: a pointer to the start of allocated chunk or NULL if it was not possible to allocate
 */
void * kernel_malloc(size_t allocs);

/*
 * kernel_realloc
 * This function reallocates a chunk of memory to larger or smaller size.
 * ptr (void *) a valid pointer to the memory which needed resize
 * resize (size_t) new size
 * returns: a pointer to the start of allocated chunk or NULL if it was not possible to allocate
 */
void * kernel_realloc(void * ptr, size_t resize);

/*
 * kernel_free
 * This function deallocates an allocated memory. This function cleans allocated memory with zeros.
 * ptr (void *) a valid pointer to the start of memory (if ptr==NULL it will return)
 * returns: nothing
 */
void kernel_free(void * ptr);

/*
 * kernel_get_total_heap
 * returns total heap size
 */
block_size_t kernel_get_total_heap();

#if KERNEL_MEM_CONT_RAM == YES
/*
 * kernel_get_total_heap_used
 * returns total heap used
 */
block_size_t kernel_get_total_heap_used();

/*
 * kernel_get_total_heap_descr
 * returns total heap descriptors
 */
block_size_t kernel_get_total_heap_descr();

/*
 * kernel_get_total_heap_fragm
 * returns total heap fragmentation
 */
block_size_t kernel_get_total_heap_fragm();
#endif

/*-----------------------------------------------------------*/
//todo
/*struct memmap
{
	block_size_t cur_address;
	block_size_t blocklen :14;
	block_size_t flag :2;
};*/

#endif /* KERNEL_MEMORY_H_ */
