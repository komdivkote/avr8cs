/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef KERNEL_PROCESS_H_
#define KERNEL_PROCESS_H_

#include "kernel_types.h"

//target's cpu frequency
#define F_CPU 16000000UL
//timer tick frequency
#define F_TICK_RATE_HZ 1000UL
//the selected timer clock prescaller
#define F_CLOCK_RESC 64

//OCR0A compare match value calculation
#define OCR_VAL_CALC (F_CPU / F_TICK_RATE_HZ) / F_CLOCK_RESC
#define OCR_B_CALC(__USEC_)  (uint32_t) __USEC_ / (uint32_t)((1.0f/(F_CPU/F_CLOCK_RESC)) * 1000000.0f)
#define SAFE_OCR0A_TCNT_MAN_SWITCH (uint8_t) OCR_VAL_CALC / 2

//process flags
#define KPROC_FLAG_STOP_MASK            0xF0    //PROCESS STOP MASK
#define KRPOC_FLAG_STOP					0		//PROCESS STOPED
#define KPROC_FLAG_RUN					1		//PROCESS RUNNING
#define KPROC_FLAG_WAIT					2		//PROCESS WAITING
#define KPROC_FLAG_SLEEP				4		//PROCESS SLEEPING
#define KPROC_FLAG_COOP					8		//COOPERATIVE FLAG
#define KPROC_FLAG_TYPE_ISR				16		//RESERVED
#define KPROC_FLAG_TYPE_SYSTEM			32
#define KPROC_FLAG_TYPE_ROUTINE			64
#define KPROC_FLAG_SIG_RELEASE			128		//RESERVED


#define SLEEP_WAKEUP_IMMID 1

//process type (RESERVED)
enum kproc_type
{
	KPROC_TYPE_SYSTEM = (uint8_t) KPROC_FLAG_TYPE_SYSTEM,
	KPROC_TYPE_ROUTINE = (uint8_t) KPROC_FLAG_TYPE_ROUTINE
};

struct kern_uptime
{
    uint16_t milis_cnt;
    uint32_t sec_cnt;
};

//process descriptor structure
struct kprocess
{
	k_pid pid;					//process ID
#if KERNEL_USE_PROCESS_TITLES == YES
	const char * title;			//title read from PROGMEM
#endif // KERNEL_USE_PROCESS_TITLES
#if KERNEL_TRACK_PROC_STATE == YES
	const char * state;			//state read from PROGMEM
#endif
	k_proc_stack * stack_start;	//start of the stack (from bottom)
	k_proc_stack * stack_end;	//end of the stack (to top)
	k_proc_stack * stack_saved;	//current stak location
	uint8_t flags;				//process flags
#if (KERNEL_MEASURE_PERF==YES)
	uint8_t tcnt0_start;		//tcnt at start
	uint32_t tm_cc;				//time consumed for 1 cycle in us
	uint8_t cpu_usage;			//usage of cpu calculated
#endif //KERNEL_MEASURE_PERF
};

//process message structure
struct kproc_msg
{
	void * out_data;	//msg to workout
	void * in_data;		//msg to pass back
};

#if KERNEL_BUILD_WITH_CONDITION_WAIT == YES
enum simple_condition_eq
{
    SC_WAKEUP_LARGER_ZERO,
    SC_WAKEUP_EQUAL_ZERO
};

struct kproc_wait_condition;

#endif // KERNEL_BUILD_WITH_CONDITION_WAIT

//extern process mutex structure
struct kproc_mutex;

/*
 * kernel_release_control should be called only when the process is running in
 *	co-operative mode and when the process is ready to release control.
 *	Calling this function from process running in pre-emptive mode will have no
 *		effect excpet it will waste the CPU time.
 *	see: ToDo!!! -> COOPERATIVE_MODE(){} (this macro is not ready)
 */
void kernel_release_control( void );

/*
 * kernel_processes_start running the scheduler and grabs the control
 */
int8_t kernel_processes_start( void );

/*
 * kernel_process_suspend
 * This function suspends process.
 *  After the successful execution of this function, the process
 *  will no longer be executed. The state will remain the same unless
 *  no other stack modifications were involved.
 * pid (k_pid) - valid process pid
 * returns: 0 on success or 1 on error
 */
int8_t kernel_process_suspend(k_pid pid);


/*
 * kernel_process_issuspended
 * This function checks if the process was suspended
 * pid (k_pid) - valid process pid
 * returns: 0 if running or 1 if stopped or -1 on error
 */
int8_t kernel_process_issuspended(k_pid pid);

/*
 * kernel_process_restart
 * This function restarts the process if it was suspended.
 *  The stack will be reseted to the initial positions and the program
 *  counter will begin from the start.
 * pid (k_pid) - valid process pid
 * (*PROCESS)(void*) (pointer PROGMEM) - pointer to the process subroutine
 * arg0 (void*) - arguments to the process
 * returns: 0 on success and 1 on error
 */
int8_t kernel_process_restart(k_pid pid, void (*PROCESS)(void*), void * arg0);

/*
 * kernel_process_remove
 * This function removes the process entirely from the SRAM:
 *  -stack
 *  -process record
 * pid (k_pid) - valid process pid
 * returns: 0 on success and 1 on error
 */
int8_t kernel_process_remove(k_pid pid);

/*
 * kernel_create_process creates the process.
 *	pm_title (const char *) PROGMEM - a process title located in program memory
 *	e_proc_type (enum kproc_type) - reserved
 *	stack_size (uint16_t) - required stack size (to the argument automatically added the register context size)
 *	PROCESS (pointer to function) - pointer to the process program
 *	arg0 (void *) - pointer to the argument
 *	returns: process id (>=0) or -1 on error
 */
k_pid kernel_create_process(const char * pm_title, enum kproc_type e_proc_type, uint16_t stack_size, void (*PROCESS)(void*), void *arg0);

/*
 * kernel_process_usleep is set the process to sleep for the requred amount
 *	of time. The argument takes time in microseconds.
 * usec (uint32_t) - microseconds (should be >= 500)
 * returns: -1 on error, 0 on success
 */
int8_t kernel_process_usleep(uint32_t usec, uint8_t flags);

/*
 * kernel_wait_for_msg_resolution blocks the process execution until the message is resolved by
 *	other process. The instance should initialliza in its stack kproc_msg and pass a pointer to it.
 * msg (struct kproc_msg *) - a message to resolve
 * returns 0 on success and 1 on error
 */
int8_t kernel_wait_for_msg_resolution(struct kproc_msg * msg);

/*
 * kernel_check_for_msg_pending checks if any unresolved message from requested PID is pending.
 * target_pid (k_pid) - process id to check
 * outdata (void **) - a pointer to pointer where the pointer to the data will be copied
 * returns 0 on success (msg pending)
 *			-1 on error
 *			1 no message pending
 */
int8_t kernel_check_for_msg_pending(k_pid target_pid, void **outdata);

/*
 * kernel_setanswer_for_msg sends back the answer and resets the destination's msg envelope.
 * target_pid (k_pid) - process id to check
 * indata (void *) - pointer to the answer, can be NULL
 * returns: 0 on success 1 on error
 */
int8_t kernel_setanswer_for_msg(k_pid target_pid, void *indata);

/*
 * This function is extremely slow because utilizes uint64_t type!
 * kernel_get_milis returns milliseconds since system was started
 * returns (k_ms) milliseconds past
 */
k_ms kernel_get_milis();

/*
 * kernel_get_seconds returns seconds since system was started
 * returns (k_sec) milliseconds past
 */
k_sec kernel_get_seconds();

#if (KERNEL_MEASURE_PERF==YES)
/*
 * kernel_get_proc_cpu_utiliz returns the % of CPU utilization by processes only!
 * returns (uint8_t) from 0 to 100 %
 */
uint8_t kernel_get_proc_cpu_utiliz();
uint32_t kernel_get_isr_cpu_msec();
uint64_t kernel_get_isr_cpu_usec();
uint32_t kernel_isr_tcnt2ms(uint32_t tcnt);

#endif // KERNEL_MEASURE_PERF

/*
 * kernel_get_current_pid returns the current instance PID.
 * returns (k_pid) current instance PID.
 */
k_pid kernel_get_current_pid(void);

/*
 * kernel_processes_get_amount returns the total amount of the processes
 * returns: (uint16_t) processes amount
 */
uint16_t kernel_processes_get_amount(void);

/*
 * kernel_processes_get_procinfo copies to the local instance of kprocess the data about
 *	selected process. The process is selected by the offset from 0 to proc_amount-1.
 *	There is no direct access to the struct kprocess list available.
 * offset (uint16_t) offset
 * envelope (struct kprocess *) pointer to instance of struct kprocess located on stack of
 *	the process.
 */
int8_t kernel_processes_get_procinfo(uint16_t offset, struct kprocess * envelope);

/*
 * kernel_get_common_stack_address returns pointer to the start of the stack of the
 *	left for the main() [entry point to the program].
 *	returns (k_proc_stack) start address.
 */
k_proc_stack * kernel_get_common_stack_address();


#if KERNEL_BUILD_WITH_CONDITION_WAIT == YES
/*
 * kernel_cond_wait_init
 * This function initializes condition which will trigger a process from a WAIT state,
 *  when condition will be true. The exec control will be granted to the process as soon
 *  as context switcher will reach it.
 * p_cvar (uint8_t*) pointer to the value which is checked to confirm if the condition is satisfied.
 * e_sceq (enum simple_condition_eq) the condition test option (trigger) i.e (p_cvar > 0) or (p_cvar == 0)
 * returns a pointer to the record located in heap or NULL
 */
struct kproc_wait_condition * kernel_cond_wait_init(uint8_t * p_cvar, enum simple_condition_eq e_sceq);

/*
 * kernel_cond_wait
 * The process is calling this function in order to check if something was changed outside of
 *  its scope. If so, the thread can continue, else it can be set to WAIT to reduce CPU usage.
 * cond (struct kproc_wait_condition *) a pointer to the created condition structure.
 * returns P_OK or P_FAIL
 */
int8_t kernel_cond_wait(struct kproc_wait_condition * cond);
#endif // KERNEL_BUILD_WITH_CONDITION_WAIT

/*
 * struct _stack_init a stack structure used for stack initialization
 */
struct _stack_init
{
	uint8_t r30; //\ Z --~\/
	uint8_t r29; ///
	uint8_t r28; //\ Y
	uint8_t r27; ///
	uint8_t r26; //\ X
	uint8_t argh;
	uint8_t argl;
	uint8_t r23;
	uint8_t r22;
	uint8_t r21;
	uint8_t r20;
	uint8_t r19;
	uint8_t r18;
	uint8_t r17;
	uint8_t r16;
	uint8_t r15;
	uint8_t r14;
	uint8_t r13;
	uint8_t r12;
	uint8_t r11;
	uint8_t r10;
	uint8_t r9;
	uint8_t r8;
	uint8_t r7;
	uint8_t r6;
	uint8_t r5;
	uint8_t r4;
	uint8_t r3;
	uint8_t r2;
	uint8_t r1;
	uint8_t f_int;
	uint8_t r0;
	uint8_t r31; ///---^
	uint8_t zero;
	uint8_t procfh;
	uint8_t procfl;
	uint8_t zero0; //????
	uint8_t procleave_hook_h;   //return address from subroutine (hook)
	uint8_t procleave_hook_l;   // low \---^
	uint8_t flag3;
	uint8_t flag2;
	uint8_t flag1;
};

#endif /* KERNEL_PROCESS_H_ */
