/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef KERNEL_TYPES_H_
#define KERNEL_TYPES_H_

#define K_NO_PID -1
typedef uint8_t k_proc_stack;
typedef int8_t k_pid;
typedef int8_t k_iid;
typedef uint64_t k_ms;
typedef uint32_t k_sec;


#endif /* KERNEL_TYPES_H_ */
