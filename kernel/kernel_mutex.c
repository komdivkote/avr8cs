/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <avr/wdt.h>
#include "kernel_sectioning.h"
#include "kernel_memory_private.h"
#include "kernel_process_private.h"

ATTRIBUTE_KERNEL_SECTION
struct kproc_mutex *
kernel_mutex_init(void)
{
	struct kproc_mutex * temp = (struct kproc_mutex *) kernel_malloc(sizeof(struct kproc_mutex));
	if (temp == NULL)
	{
		return NULL;
	}

	memset(temp, 0, sizeof(struct kproc_mutex));

	return temp;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_mutex_lock(struct kproc_mutex * mutx)
{
	if (mutx == NULL)
	{
		return -1;
	}

	KERNEL_SET_STATE(KSTATE_MUX)
	{
		//switching to cooperative mode to prevent other processes form losing sync
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			//__set_proc_state(KSTATE_MUX);
			current_kprocess->node->flags |= KPROC_FLAG_COOP;
		}

        //check if mutex already loacked by currentthread
        if (mutx->owner == current_kprocess->node)
        {
            goto leave_locked;
        }

		//check if mutex is free, someone else waiting for it
		if ((mutx->owner == NULL) && (mutx->c_waiting == 0))
		{
			//no one waiting, locking
			mutx->owner = current_kprocess->node;
			goto leave_locked; //mutex locked

		}

		//-----mutex is busy!

		//setting the process to wait
		current_kprocess->node->flags |= KPROC_FLAG_WAIT;
		//increasing the waiting counter for the mutex
		current_kprocess->mutex_wait_order = mutx->c_mutx_order;
		current_kprocess->wait_mutex = mutx;

		//incrementing counters
		++mutx->c_waiting;
		if (mutx->c_mutx_order == 255)
		{
			mutx->c_mutx_order = 0;
		}
		else
		{
			++mutx->c_mutx_order;
		}

		//releasing control
		kernel_release_control();

		//we got control back, it mean that mutex is free now!
		current_kprocess->wait_mutex->owner = current_kprocess->node;
		//substract waiting
		--current_kprocess->wait_mutex->c_waiting;
		//reset field "waiting"
		current_kprocess->wait_mutex = NULL;
		current_kprocess->mutex_wait_order = 0;
		//leaving cooperative mode (the sleep flag is dropped by scheduler)

		leave_locked:
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			current_kprocess->node->flags &= ~KPROC_FLAG_COOP;
			//__set_proc_state(KSTATE_CPU);
		}
	}
	return 0;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_mutex_unlock(struct kproc_mutex * mutx)
{
    if (mutx == NULL)
	{
		return -1;
    }

	KERNEL_SET_STATE(KSTATE_MUX)
	{

		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			//mutx->prev_owner = mutx->owner;
			mutx->owner = NULL;
		}
	}

	return 0;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_mutex_try_lock(struct kproc_mutex * mutx)
{
	uint8_t status = 0;

	if (mutx == NULL)
	{
		return -1;
	}

	KERNEL_SET_STATE(KSTATE_MUX)
	{
		//switching to cooperative mode to prevent other processes form losing sync
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			//__set_proc_state(KSTATE_MUX);
			current_kprocess->node->flags |= KPROC_FLAG_COOP;
		}

		//check if mutex is free, someone else waiting for it
		if ((mutx->owner == NULL) && (mutx->c_waiting == 0))
		{
			//no one waiting, locking
			mutx->owner = current_kprocess->node;
			status = 0;

		}
		else
		{
			status = 1;
		}

		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			//__set_proc_state(KSTATE_CPU);
			current_kprocess->node->flags &= ~KPROC_FLAG_COOP;
		}
	}
	return status;
}

ATTRIBUTE_KERNEL_SECTION
inline k_pid
kernel_mutex_get_cnt(struct kproc_mutex * mux)
{
	return mux->owner->pid;
}

ATTRIBUTE_KERNEL_SECTION
inline uint8_t
kernel_mutex_get_cwating(struct kproc_mutex * mux)
{
	return mux->c_waiting;
}

ATTRIBUTE_KERNEL_SECTION
inline uint8_t
kernel_mutex_get_corder(struct kproc_mutex * mux)
{
	return mux->c_mutx_order;
}

ATTRIBUTE_KERNEL_SECTION
inline uint8_t
kernel_mutex_get_ccounter(struct kproc_mutex * mux)
{
	return mux->c_mutx_counter;
}

ATTRIBUTE_KERNEL_SECTION
struct kproc_mutex *
kernel_mutex_get_proc_waiting(k_pid pid)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        volatile struct kprocess_list * volatile temp = kproc_list;
        while (temp != NULL)
        {
            if (temp->node->pid == pid)
            {
                return temp->wait_mutex;
            }
            temp = temp->next;
        }
    }

	return NULL;
}
